<?php

use Facebook\WebDriver\WebDriver;
use Facebook\WebDriver\WebDriverBy;

/**
 * Created by PhpStorm.
 * User: zet
 * Date: 31.05.17
 * Time: 18:38
 */
class GooglePage
{
    /**
     * GooglePage constructor.
     */
    public function __construct(Webdriver $driver)
    {
        $this->driver = $driver;
    }

    //Method which return number of site in google search rating.
    public function googleSearchResultPlace()
    {
        $position = 1;
        $value = "ortnec.com/";

        $openGooglePage = new OpenGooglePage($this->driver);
        $elements = $openGooglePage->OpenGooglePageWithSearch();

        for ($i = 1; $i < count($elements); $i++) {
            $element_value = $this->driver->findElement(WebDriverBy::xpath("//div[@class='g'][" . $i . "]//cite[@class='_Rm']"))->getText();
            if ($value == $element_value) {
                return $position++;
            }
        }
    }

    //Method which verify word "Ortnec" on each site.
    public function  clickEachLinkAndVerifyWord(){
        $count = 1;
        $openGooglePage = new OpenGooglePage($this->driver);
        $elements = $openGooglePage->OpenGooglePageWithSearch();
        for($i = 1; $i < count($elements); $i++){
            $this->driver->findElement(WebDriverBy::xpath("//div[@class='g'][". $i ."]//h3//a"))->click();
            $this->driver->manage()->timeouts()->implicitlyWait(5);
            $pageSource = $this->driver->getPageSource();
            if(strpos($pageSource, "ortnec", true)){
                $count++;
            }
            $this->driver->navigate()->back();
        }
        return $count;
    }
}