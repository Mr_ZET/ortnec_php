<?php

use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;

class Main extends \PHPUnit\Framework\TestCase
{
    private $webDriver;

    public function setUp()
    {
        $this->webDriver = RemoteWebDriver::create('http://localhost:4444/wd/hub', DesiredCapabilities::chrome(),5000);
        $this->webDriver->manage()->window()->maximize();
        $this->webDriver->manage()->timeouts()->implicitlyWait(5);
    }

    public function tearDown()
    {
        $this->webDriver->quit();
    }

    //This test return number of site "Ortnec" in google search.
    public function testA(){
        $goooglepage = new GooglePage($this->webDriver);
        $assert = $goooglepage->googleSearchResultPlace();
        $this->assertEquals($assert > 0, true, "Ortnec company has position №" . $assert . " in google search results");
        echo "Ortnec company has position №" . $assert . " in google search results";
    }

    //This test click on each site which presented in google search results
    //And verifying text "Ortnec" on those sites.
    public function testB(){
        $googlepage = new GooglePage($this->webDriver);
        $assert = $googlepage->clickEachLinkAndVerifyWord();
        $this->assertEquals($assert, true,"About Ortnec written on " . $assert . " sites of 10");
        echo "About Ortnec written on " . $assert . " sites of 10";
    }
}