<?php

use Facebook\WebDriver\WebDriver;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverKeys;

/**
 * Created by PhpStorm.
 * User: zet
 * Date: 31.05.17
 * Time: 18:40
 */
class OpenGooglePage
{
    /**
     * OpenGooglePage constructor.
     */
    public function __construct(WebDriver $driver)
    {
        $this->driver = $driver;
    }

    //Method which opens google page and set appropriate value into search field and return results.
    public function OpenGooglePageWithSearch(){
        $this->driver->navigate()->to("https://google.com/");
        $search_field = $this->driver->findElement(WebDriverBy::id('lst-ib'));
        $search_field->sendKeys('Ortnec');
        $search_field->sendKeys(WebDriverKeys::ENTER);
        $this->driver->manage()->timeouts()->implicitlyWait(5);
        $elements = $this->driver->findElements(WebDriverBy::xpath("//div[@class='g']"));

        return $elements;
    }
}